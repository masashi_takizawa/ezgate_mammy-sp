# gulpfile.coffee: build script for front assets
#
# gulp        - build assets
# gulp watch  - build assets continuously
# gulp server - start a server with assets and mocked APIs


# node_modules
bower       = require 'bower'
del         = require 'del'
gulp        = require 'gulp'
concat      = require 'gulp-concat'
sass        = require 'gulp-sass'
bulkSass    = require 'gulp-sass-bulk-import'#sassで*を使えるようにする
cssnext     = require 'gulp-cssnext'
uglify      = require 'gulp-uglify'
plumber     = require 'gulp-plumber' 
browserSync = require 'browser-sync'
autoprefixer= require 'gulp-autoprefixer'
changed     = require 'gulp-changed'
ejs         = require 'gulp-ejs'
imagemin    = require 'gulp-imagemin'


sources =
  bower:  'bower.json'
  mainjs: 'src/mainjs/*.js'#メインのJSファイル
  script: 'src/js/*.js'#プラグイン要JSなど配置
  scss:   'src/scss/**/*.scss'
  static: 'public/**/*'#HTMLなど
  ejs: 'ejs/**/*.ejs'
  ejsTemplate: 'ejs/**/_*.ejs'

libs =
  #結合しないjs
  js: [
    'jquery/jquery.min.js'
    'jquery-mobile-bower/js/jquery.mobile-1.4.5.js'
    'bxslider-4/dist/jquery.bxslider.min.js'
    'matchHeight/dist/jquery.matchHeight-min.js'
    'jQuery-Validation-Engine/js/jquery.validationEngine.js',
    'jQuery-Validation-Engine/js/languages/jquery.validationEngine-ja.js'
  ]
  #結合するjs
  concatJs: [
  ]
  css:    [
    'jQuery-Validation-Engine/css/validationEngine.jquery.css'
    'jquery-mobile-bower/css/jquery.mobile.structure-1.4.5.min.css'
    'font-awesome/css/font-awesome.min.css'
    'bxslider-4/dist/jquery.bxslider.min.css'
    ]
  #/fontフォルダ
  fonts: [
    'font-awesome/fonts/**/*'
  ]
  #/css/fontsフォルダ
  cssFonts: [
  ]
  cssStatic:[
  ]
  #/css/imagesフォルダ
  cssImages: [
    'jquery-mobile-bower/css/images/**/*'
    'bxslider-4/dist/images/**/*'
  ]

gulp.task 'clean', (cb) ->
  del 'deploy/', cb

gulp.task 'watch', ->
  gulp.watch sources.bower,  ['compile:lib']
  gulp.watch sources.mainjs, ['compile:mainjs']
  gulp.watch sources.script, ['compile:script']
  gulp.watch sources.scss,   ['compile:scss']
  gulp.watch sources.static, ['compile:static']
  gulp.watch [sources.ejs,sources.ejsTemplate],['compile:ejs']

gulp.task 'compile:ejs', ->
    gulp.src [sources.ejs,'!'+sources.ejsTemplate],{base: './ejs/'}
      .pipe ejs({}, {ext: '.html'})
      .pipe gulp.dest 'deploy/'
      .pipe browserSync.reload stream:true

gulp.task 'compile:lib', ->
  bower.commands.install().on 'end', ->
    gulp.src libs.js.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe uglify()
      .pipe gulp.dest 'deploy/js/'
    gulp.src libs.concatJs.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe uglify()
      .pipe concat 'lib.js'
      .pipe gulp.dest 'deploy/js/'
    gulp.src libs.css.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe gulp.dest 'deploy/css/'
    gulp.src libs.fonts.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe gulp.dest 'deploy/fonts'
    gulp.src libs.cssFonts.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe gulp.dest 'deploy/css/fonts/'
    gulp.src libs.cssImages.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe gulp.dest 'deploy/css/images/'
      gulp.src libs.cssStatic.map (e) -> "bower_components/#{e}"
      .pipe plumber()
      .pipe gulp.dest 'deploy/css/'

gulp.task 'compile:mainjs', ->
  gulp.src sources.mainjs
    .pipe plumber()
    .pipe uglify()
    .pipe concat 'script.js'
    .pipe gulp.dest 'deploy/js/'
    .pipe browserSync.reload stream:true

gulp.task 'compile:script', ->
  gulp.src sources.script
    .pipe plumber()
    .pipe uglify()
    .pipe concat('modules.js')
    .pipe gulp.dest 'deploy/js/'
    .pipe browserSync.reload stream:true

gulp.task 'compile:scss', ->
  gulp.src sources.scss
    .pipe plumber()
    .pipe bulkSass()
    .pipe sass().on 'error', sass.logError
    .pipe cssnext(
      features:{
        rem:false
      },
      browser: ['last 2 versions', 'ie 9']
    )
    .pipe gulp.dest 'deploy/css/'
    .pipe browserSync.reload stream:true

gulp.task 'compile:static', ->
  gulp.src sources.static
    .pipe plumber()
    .pipe changed 'deploy/'
    .pipe imagemin()
    .pipe gulp.dest 'deploy/'
    .pipe browserSync.reload stream:true

gulp.task 'browserSync', ->
	browserSync.init(null,{
		# open: false
		#proxy: 'localhost/対象ディレクトリ'
		server:
			baseDir:'./deploy/'
	})

gulp.task 'browserSync-Reload', ->
	browserSync.reload()

gulp.task 'default', ['browserSync','compile:lib','compile:ejs', 'compile:mainjs','compile:script','compile:scss', 'compile:static'], ->
	gulp.start 'watch'
